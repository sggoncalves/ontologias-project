"""

Projecto de Ontologias aplicadas às Ciências 2018/2019
@author Sara Gonçalves nº fc50254


"""

import os, csv, requests,sys
import pandas as pd
import xml.etree.ElementTree as ET
import merpy


# ------------------ GET FUNCTIONS

def select_genes_from_annotated_data(data_path, number = 9):

    """


    :param data_path:
    :param number:
    :return: list of genes
             list of associations [{"gene_id":1, "phen_id":1, "phen_name":"x","gene_name":"x"}]

    """

    data_dir = os.getcwd() + "/" + data_path
    genes_list = []
    association_list = []
    counter = 0

    for file in os.listdir(data_dir):

        doc = data_dir +"/"+ file

        # using pandas to read from file
        df = pd.read_csv(doc, sep="\t")

        for aux in df.values:

            genes_association = {}
            data_line = []

            auxi = aux[0].split(' ')
            genes_list.append(auxi[0])

            for i in auxi:
                if i != "":
                    data_line.append(i)


            ## ---------------- parsing helper

            gene_name = data_line[0]
            gene_id = data_line[-2]
            phen_id = data_line[-1]

            phen_name = ""
            for i in range(1,len(data_line)-2):
                phen_name += data_line[i] + " "

            phen_name = phen_name[0:-1]

            # ---------------------------


            # the association of each gene
            association = {"gene_id":gene_id, "gene_name":gene_name, "phen_id":phen_id, "phen_name":phen_name}

            # incrementing counter
            counter += 1

            # appending to associations list
            association_list.append(association)

            # returning when reaches the number of genes
            if counter == number:
                return genes_list,association_list

def get_pubmed_abstract(pmid):

    """
    :param pmid: pubmed article id
    :return: title, abstract, response status_code
    """
    payload = {"db": "pubmed", "id": str(pmid), "retmode": "xml", "rettype": "xml"}
    r = requests.get('http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi', payload)
    response = r.text

    if r.status_code != 200:
        raise Exception

    try:

        title, abstract = parse_pubmed_xml(response)
        return title, abstract, str(r.status_code)

    except:
        raise Exception

def get_pubmed_articles_by_gene_name(name,nr = 50):

    """

    :param name: gene name
    :param nr: number of results
    :return: list of ids

    """

    payload = {"db": "pubmed", "term": name, "retmode": "xml", "retmax": nr}
    r = requests.get('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi', payload)
    response = r.text

    if r.status_code != 200:
        raise Exception

    ids_list = parse_pubmed_list_xml(response)
    return ids_list


# ------------------ PARSING FUNCTIONS


def parse_pubmed_xml(xml):

    """
    :param xml: xml format response from pubmed
    :return: title, abstract text
    """


    if xml.strip() == '':
        print("PMID not found")
        raise Exception

    else:
        root = ET.fromstring(xml.encode("utf-8"))
        title = root.find('.//ArticleTitle')
        if title is not None:
            title = title.text
        else:
            title = ""
        abstext = root.findall('.//AbstractText')
        if abstext is not None and len(abstext) > 0:
            abstext = [a.text for a in abstext]
            if all([abst is not None for abst in abstext]):
                abstext = '\n'.join(abstext)
            else:
                abstext = ""
        else:
            raise Exception

    return title, abstext

def parse_pubmed_list_xml(xml):

    """

    Parser of Pubmed id's from get list of pubmed xml

    :param xml: xml format response from pubmed
    :return: title, abstract text
    """

    ids_list = []

    if xml.strip() == '':
        print("No PMID's for this gene")
        raise Exception

    else:
        root = ET.fromstring(xml.encode("utf-8"))
        id = root.findall('.//Id')
        if id is not None and len(id) > 0:
            # getting the Id tag content
            ids_list = [i.text for i in id]

        else:
            print("No ids associated with this gene")
            raise Exception

    return ids_list


# ------------------ AGGREGATOR FUNCTIONS

def fetch_pubmed_from_genes_list(genes_list,narticles = 2):
    """
    :param genes_list: list of genes names
    :param narticles: number of articles per gene

    :return:    data_fetched = {
                    "GRXCR1":[
                        {"title":"teste", "abstract":"teste"},
                        {"title":"teste", "abstract":"teste"}
                        ]
        }

    """

    data  = {}

    for gene in genes_list:

        if gene in data:
            print("This gene was already fetched")
            continue

        else:

            data[gene] = []


            for pmid in get_pubmed_articles_by_gene_name(gene):

                if len(data[gene]) >= narticles:
                    break

                try:

                    title, abstract, status = get_pubmed_abstract(pmid)
                    print("fetching pubmed data for gene: {} - pmid: {}".format(gene, pmid))
                    aux = {'title': title, 'abstract': abstract, "pmid": pmid}
                    data[gene].append(aux)
                    aux = {}

                except:

                    print("skipping pmid {} for gene {}".format(pmid,gene))
                    continue

    return data

def compare_dishin(term1,term2):

    command = "python ./DiShIn/dishin.py ./DiShIn/hp.db {} {}".format(term1,term2)
    return os.system(command)


def main():


    # printing configs
    final_stats = True
    mer_detail = True
    dishin = False

    data_path = "./data"

    # selecting genes
    genes_list, ph_data = select_genes_from_annotated_data(data_path,11)

    # fetching data
    pm_data = fetch_pubmed_from_genes_list(genes_list)


    print("-" * 20)
    print("\n Starting MER Analysis \n")
    print("-" * 20)

    for association in ph_data:

        gene_name = association['gene_name']
        phen_name = association['phen_name']
        association['stats'] = []
        association['entities'] = []

        for a in range(0,len(pm_data[association['gene_name']])):

            text = pm_data[association['gene_name']][a]['abstract']
            entities = merpy.get_entities(text, "hpo")
            

            if mer_detail:
                print("\n ", "gene: ", association['gene_name'], "phenotype: ", phen_name, "\n")
                print("text: ", text)
                print("-" * 20)
                print("\nPhenotype: ", phen_name)
                print("PMID: ", pm_data[association['gene_name']][a]['pmid'])
                print("Gene:", association['gene_name'],'\n')
                print("-" * 20)
                print("Entities found MER:\n")

            # ----- statistics helpers
            equal = 0
            into = 0
            unique = 0
            elen = 0
            precision = 0

            try:

                for e in range(0,len(entities)):

                    ent = entities[e][2]

                    if mer_detail:

                        print("{} ({},{})".format(entities[e][2], entities[e][0], entities[e][1]))

                    # discarting repeated entities for statistics
                    aux = False
                    for i in  association['entities']:
                        if i.lower() == ent.lower():
                            aux = True
                            break

                    # doesn't make the next steps if it is already on entities list
                    if aux:
                        continue

                    elen += 1

                    # updating statists
                    if ent == phen_name:
                        equal += 1

                    elif ent in phen_name or phen_name in ent:
                        into += 1


                    if ent not in association['entities']:
                        association['entities'].append(ent)


                # Stats
                if elen != 0:
                    precision = round(equal / elen, 2) * 100
                print("\n Gene: {} Phenotype: {} \n Equivalents: {}, Containing: {} , # Entities: {}, Precision: {}".format(association['gene_name']
                                                                                         ,association['phen_name'],equal, into,
                                                                                         elen,str(precision) + "%"))


                association['stats'].append({

                    "equivalents": equal, "contained": into, "total": len(entities), "precision": precision

                })



            except:
                print("No entities found gene:", gene_name)


    if final_stats:

        print("-" * 20)
        print("final statistics")
        print("-" * 20,"\n")

        print("\t {:10s} \t {:10s} \t {:15s} \t {:10s} ".format("gene name", "phen id ","avg precision","phenotype name"))

        for i in ph_data:
            for j in range(0,len(i['stats'])):
                print("\t {:10s} \t {:10s} \t {:15s} \t {:10s} ".format(str(i['gene_name']),str(i['phen_id']),str(i['stats'][j]['precision']),str(i['phen_name'])))

            print("\n", association['entities'])


def dishin_manual():

    dishin_data = []
    file = "helper.csv"
    with open(file) as f:
        next(f)
        for i in f.readlines():
            aux = i.split(",")

            ## extracting data from csv
            nr = aux[0]
            gene = aux[1]
            ori_phen_name = aux[2]
            ori_phen_id = aux[3]
            pmid = aux[4]
            mer_phen_name = aux[5]
            mer_phen_id = aux[6]
            manual = aux[7]
            notes = aux[8]

            # printing dishin output
            print("\n #: {} Gene: {} Phenotype ori: {} Phenotype hip: {} \n ".format(nr,gene,ori_phen_name,mer_phen_name))


            if mer_phen_id != "-":
                print(compare_dishin(ori_phen_id,mer_phen_id))

            if notes != " ":
                print("\n Notes: {}".format(notes))




if __name__ == '__main__':

    dishin_manual()

    #main()

